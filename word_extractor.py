import os
import sys
import argparse


from six import text_type

def extractor(args):
	command = "python3 sample.py --save_dir word_predict --prime \"" + args.prime + "\""
	result = os.popen(command).read()
	return result

# print(result)
def main():
	os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
	parser = argparse.ArgumentParser(
	                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('--prime', type=text_type, default=u' ',
	                        help='prime text')
	args = parser.parse_args()

	result = extractor(args)

	print(result.split("\n")[0].split(" ")[len(args.prime.split())])


if __name__ == '__main__':
    main()






